from setuptools import setup, find_packages

setup(
    name="pylossy",
    version="1.0",
    description="Utility to compress data in bytearrays with given resolution",
    author="Borja Fons",
    author_email="borjafons@gmail.com",
    packages=find_packages(exclude=["tests"]),
)
